This is the public repository for the Calibre SVRF to KLayout/Cadence PVS translator.
It supports a subset of DRC and LVS commands. The tool is shipped as a universal binary
for node.

To translate to PVS:

node hdrc.js -pvs calibre.drc

The output file is klayout.drc

To translate to KLayout:

node hdrc.js -klayout calibre.drc

The output file is pvs.rul

This version is limited to an output of 1000 commands for PVS output.
